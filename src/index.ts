import dotenv from "dotenv";
dotenv.config({ path: "../.env" });

import { Options } from "graphql-yoga";
import { createConnections } from "typeorm";

import app from "./app";



const PORT: number | string = process.env.PORT || 4000;
const PLAYGROUND_ENDPOINT: string = "/playground";
const GRAPHQL_ENDPOINT: string = "/graphql";

const appOptions: Options = {
  port: PORT,
  playground: PLAYGROUND_ENDPOINT,
  endpoint: GRAPHQL_ENDPOINT,
  cors: {
    credentials: true,
    origin: true
  }
};

const handleAppStart = () => console.log(`Server is Listening on ${PORT}`);



createConnections()
  .then(() => {
    app.start(appOptions, handleAppStart);
  })
  .catch(err => console.log(err));

