import User from "../../../entities/FirstDemo/User";
import { GetMyProfileResponse } from "../../../types/graph";
import { Resolvers } from "../../../types/resolvers";
import privateResolver from "../../../utils/privateResolver";
import { getMongoManager } from 'typeorm';
import postModal from '../../../entities/SecondDemo/post'


const resolvers: Resolvers = {
  Query: {
    GetMyProfile: privateResolver(
      async (_, __, { req }): Promise<GetMyProfileResponse> => {
        const user: User = req.user;
        const manger = getMongoManager("mongo")
        try {
          const profile = await User.findOne({ id: user.id });
        
         

      

          let skip1 = (0-1)*10;
          console.log(skip1)
          let limit1 = 10;

          console.log(skip1)
          const [PostsData, total]:any  = await manger.getRepository(postModal).findAndCount({
            take:limit1,
            skip:skip1
          });

          let totalPages:number = Math.ceil(total / limit1);


            console.log(PostsData,total)

          if (profile) {
            return {
              ok: true,
              error: null,
              profile,
              PostsData,
              total,
              totalPages
            };
          } else {
            return {
              ok: false,
              error: "User Not Found",
              profile: null,
              PostsData,
              total:null,
              totalPages:null
            };
          }
        } catch (error) {
          return error.message
        }
      }
    )
  }
};

export default resolvers;
