import User from "../../../entities/FirstDemo/User";
import { Resolvers } from "../../../types/resolvers";
import createJWT from "../../../utils/createJWT";
import {
  EmailSignInMutationArgs,
  EmailSignInResponse
} from "./../../../types/graph.d";
import { getManager } from "typeorm";
import simpelDemo from "../../../entities/SecondDemo/simpelDemo";

const resolvers: Resolvers = {
  Mutation: {
    EmailSignIn: async (
      _,
      args: EmailSignInMutationArgs
    ): Promise<EmailSignInResponse> => {
      const { email, password } = args;

      const manager = getManager("mongo"); // or connection.manager

      const timber1 = await manager.find(simpelDemo);
      await manager.save(simpelDemo, { email: "abc@gmail.com", title: "Hello" })
      console.log("timber", timber1)

      try {

        const user = await User.findOne({ email });

        // if the email isn't found from User
        if (!user) {
          return {
            ok: false,
            error: "there is no account with that email, please sign up first",
            token: null
          };
        }

        const passwordCheck = await user.comparePassword(password);

        if (passwordCheck) {
          const token = await createJWT(user.id);

          return {
            ok: true,
            error: null,
            token
          };
        } else {
          return {
            ok: true,
            error: "wrong password",
            token: null
          };
        }
      } catch (error) {
        return {
          ok: false,
          error: error.message,
          token: null
        };
      }
    }
  }
};

export default resolvers;
