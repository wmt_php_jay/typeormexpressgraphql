import { IsEmail } from "class-validator";
import {
  BaseEntity,
  Column,
  Entity,
  ObjectID,
  ObjectIdColumn
  
} from "typeorm";



@Entity({database:"firstdemo"})
class simpelDemo extends BaseEntity {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({ type: "text", nullable: true })
  @IsEmail()
  email: string;

  @Column({ type: "text", nullable: true })
  title: string;


  
}

export default simpelDemo;
