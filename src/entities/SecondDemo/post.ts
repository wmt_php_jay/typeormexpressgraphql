import {
  BaseEntity,
  Column,
  Entity,
  ObjectID,
  ObjectIdColumn,
 } from "typeorm";



@Entity({database:"firstdemo",name:"posts"})
class Post extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column({ type: "text", nullable: false })
  title: string;

  @Column({ type: "text", nullable: true })
  description: string;
  
  @Column({ type: "text", nullable: true })
  user_id: string;

}

export default Post;
