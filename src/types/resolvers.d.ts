export type Resolver = (parent: any, args: any, context: any, info: any) => any;
export type Subscription = (parent: any, args: ant, context: any, info:any) => any;

export interface Resolvers {
  [key: string]: {
    [key: string]: Resolver;
  };
}

export interface Subscription {
  [key: string]: {
    [key: string]: Subscription;
  };
}
