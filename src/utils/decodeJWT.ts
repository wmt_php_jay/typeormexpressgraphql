import jwt from "jsonwebtoken";
import User from "../entities/FirstDemo/User";

const decodeJWT = async (token: string): Promise<User | undefined> => {

  try {
    const s = token.split(" ")
    const decodedToken: any = jwt.verify(s[1], "cFkSj6lgNvpXxJEw5JsAwFWBK2FqKuDA");
    const { id } = decodedToken;
    const user = await User.findOne({ id });
    if (user) {
      return user;
    } else {
      return undefined;
    }
  } catch (error) {
    return undefined;
  }
};

export default decodeJWT;
