import jwt from "jsonwebtoken";

const createJWT = (id: number): string => {
  const token = jwt.sign({ id }, "cFkSj6lgNvpXxJEw5JsAwFWBK2FqKuDA");
  
  return token;
};

export default createJWT;
